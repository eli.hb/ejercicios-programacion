/*
	Name: Programa que calcula el perimetro de un triangulo Equilatero
	Copyright: Elidez2020
	Author: Eli Hernandez Ballesteros
	Date: 14/08/20 20:51
*/
#include <stdio.h>
int main(){
	
	float L,B,P;// se ocupara la letra L para referirse a los  Lados, la B para referirse a la base y la P pra Perimetro.
	printf("Este Programa calcula el perimetro de un triangulo Isosceles\n");
	printf("Ingrese el valor de uno de los lados\n");
	scanf("%f", &L);
	printf("Ingrese el valor de la base\n");
	scanf("%f", &B);
	P=L*2+B;
	printf("El perimetro del triangulo seria: %.2lf",P);
	
}
