/*
	Name: Programa que calcula el perimetro de un triangulo Escaleno
	Copyright: Elidez2020
	Author: Eli Hernandez Ballesteros
	Date: 14/08/20 20:51
*/
#include <stdio.h>
int main(){
	
	float L1,L2,B,P;// se ocupara la letra L para referirse a los  Lados, la B para referirse a la base y la P pra Perimetro.
	printf("Este Programa calcula el perimetro de un triangulo Escaleno\n");
	printf("Ingrese el valor del primer lado\n");
	scanf("%f", &L1);
	printf("Ingrese el valor del segundo lado\n");
	scanf("%f", &L2);
	printf("Ingrese el valor de la base\n");
	scanf("%f", &B);
	P=L1+L2+B;
	printf("El perimetro del triangulo seria: %.2lf",P);
	
}
