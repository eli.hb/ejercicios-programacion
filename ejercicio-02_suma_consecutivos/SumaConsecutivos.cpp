/*
	Name:Ejercicio02-sumaConsecutivos 
	Copyright:Edez2020 
	Author: Eli Hernandez
	Date: 14/08/2020
*/
#include <stdio.h>
int main(){

int numero;
printf("Dame un numero del 1 al 50:\n");
scanf("%d", &numero);
if (numero<=50){
	numero=(numero*(numero+1)/2);
		printf("la suma consecutiva hasta ese numero es: %d",numero);
}else {
	printf("El numero es mayor a 50");
}
return 0;
}


