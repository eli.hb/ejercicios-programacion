/*
	Name: Programa que calcula el perimetro de un triangulo Equilatero
	Copyright: Elidez2020
	Author: Eli Hernandez Ballesteros
	Date: 14/08/20 20:51
*/
#include <stdio.h>
int main(){
	
	float L;// se ocupara la letra L para referirse a los  Lados 
	printf("Este Programa calcula el perimetro de un triangulo equilatero\n");
	printf("Ingrese el valor de uno de los lados\n");
	scanf("%f", &L);
	L=L*3;
	printf("El perimetro del triangulo seria: %.2lf",L);
	
}
